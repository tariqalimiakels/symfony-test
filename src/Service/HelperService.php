<?php

namespace App\Service;

use App\Request\BaseRequest;
use Carbon\Carbon;

class HelperService extends BaseRequest
{
    /**
     * Get diff in minutes
     *
     * @return number
     */
    public function getDiffInMinutes($oldDateTime)
    {
        $date = Carbon::parse($oldDateTime);
        $now = Carbon::now();

        return $date->diffInMinutes($now);
    }
}
