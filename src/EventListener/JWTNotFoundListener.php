<?php

namespace App\EventListener;

use App\Controller\ApiController;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;


class JWTNotFoundListener extends ApiController {

    /**
     * @param JWTNotFoundEvent $event
     */
    public function onJWTNotFound(JWTNotFoundEvent $event)
    {

        $response = $this->setStatusCode(401)->respondWithErrors("Missing token");
        $event->setResponse($response);
    }
}