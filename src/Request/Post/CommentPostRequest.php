<?php

namespace App\Request\Post;

use App\Entity\Post;
use App\Entity\User;
use App\Exception\CustomException;
use App\Exception\ValidationException;
use App\Repository\CommentRepository;
use App\Request\BaseRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\RequestStack;

class CommentPostRequest extends BaseRequest
{
    private $validator;
    private $request;
    private $manager;
    private $commentRepository;

    public function __construct(RequestStack $requestStack, ValidatorInterface $validator, EntityManagerInterface $manager, CommentRepository $commentRepository)
    {
        $this->validator = $validator;
        $this->commentRepository = $commentRepository;
        $this->manager = $manager;
        $request = $requestStack->getCurrentRequest();
        $this->request = $this->transformJsonBody($request);
    }

    public function addComment($userId)
    {
         //Validate
         $errors = $this->validate($this->request);
         if (count($errors) > 0) {
            throw new ValidationException($errors);
         }

         $postId = $this->request->get('post_id');
         $comment = $this->request->get('comment');
         $user = $this->manager->getRepository(User::class)->find($userId);
         $post = $this->manager->getRepository(Post::class)->find($postId);

         if (!$post) {
            throw new CustomException('Post not found!');
         }

         //Save
         $this->commentRepository->saveComment($comment, $user, $post);
    }

    private function validate(Request $request)
    {
        //Validate
        $constraints = new Collection(array(
            'post_id' => new Assert\NotBlank(),
            'comment' => new Assert\NotBlank(),
        ));

         $errors = $this->validator->validate(
             $request->request->all(),
             $constraints
         );

         return $errors;
    }
}
