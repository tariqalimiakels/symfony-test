<?php

namespace App\Request\Post;

use App\Exception\CustomException;
use App\Repository\PostRepository;
use App\Request\BaseRequest;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Service\HelperService;

class DeletePostRequest extends BaseRequest
{
    private $postRepository;
    private $helperService;

    public function __construct(RequestStack $requestStack, PostRepository $postRepository, HelperService $helperService)
    {
        $this->postRepository = $postRepository;
        $this->helperService = $helperService;
        $request = $requestStack->getCurrentRequest();
        $this->request = $this->transformJsonBody($request);
    }

    public function delete($id)
    {
        $post = $this->postRepository->findOneBy(['id' => $id]);
        if (!$post) {
            throw new CustomException('Post not found!');
        }

        //Check if 1 hour has been passed
        $diffInMin = $this->helperService->getDiffInMinutes($post->getUpdatedAt());

        if ($diffInMin > 60) {
            throw new CustomException('Time expired. Post cannot be deleted!');
        }

        return $this->postRepository->deletePost($post);
    }
}
