<?php

namespace App\Request\Post;

use App\Entity\User;
use App\Exception\CustomException;
use App\Exception\ValidationException;
use App\Repository\PostRepository;
use App\Request\BaseRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Service\HelperService;

class SavePostRequest extends BaseRequest
{
    private $postRepository;
    private $validator;
    private $manager;
    private $helperService;
    private $request;

    public function __construct(RequestStack $requestStack, ValidatorInterface $validator, EntityManagerInterface $manager, PostRepository $postRepository, HelperService $helperService)
    {
        $this->postRepository = $postRepository;
        $this->validator = $validator;
        $this->manager = $manager;
        $this->helperService = $helperService;
        $request = $requestStack->getCurrentRequest();
        $this->request = $this->transformJsonBody($request);
    }

    public function add($userId)
    {
        //Validate
        $errors = $this->validatePost($this->request);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        $user = $this->manager->getRepository(User::class)->find($userId);
        $description = $this->request->get('description');

        //Save
        $this->postRepository->savePost($description, $user);
    }

    public function update(int $id)
    {
        //Validate
        $errors = $this->validatePost($this->request);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        $post = $this->postRepository->find($id);
        if (!$post) {
            throw new CustomException('Post not found!');
        }

        //Check if 1 hour has been passed
        $diffInMin = $this->helperService->getDiffInMinutes($post->getUpdatedAt());

        if ($diffInMin > 60) {
            throw new CustomException('Time expired. Post cannot be updated!');
        }

        //Save
        $post->setDescription($this->request->get('description'));
        $this->postRepository->updatePost($post);
    }

    private function validatePost(Request $request)
    {
        //Validate
        $constraints = new Collection(array(
            'description' => new Assert\NotBlank(),
        ));

        $errors = $this->validator->validate(
            $request->request->all(),
            $constraints
        );

        return $errors;
    }
}
