<?php

namespace App\Request\Post;

use App\Entity\Post;
use App\Entity\User;
use App\Exception\CustomException;
use App\Exception\ValidationException;
use App\Repository\UserRepository;
use App\Request\BaseRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\RequestStack;

class LikePostRequest extends BaseRequest
{
    private $validator;
    private $request;
    private $manager;
    private $userRespository;

    public function __construct(RequestStack $requestStack, ValidatorInterface $validator, EntityManagerInterface $manager, UserRepository $userRespository)
    {
        $this->validator = $validator;
        $this->userRespository = $userRespository;
        $this->manager = $manager;
        $request = $requestStack->getCurrentRequest();
        $this->request = $this->transformJsonBody($request);
    }

    public function addOrRemoveLike($userId)
    {
        //Validate
        $errors = $this->validate($this->request);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }
        
        $postId = $this->request->get('post_id');
        $user = $this->manager->getRepository(User::class)->find($userId);
        $post = $this->manager->getRepository(Post::class)->findOneBy(['id' => $postId]);

        if (!$post) {
            throw new CustomException('Post not found!');
        }

        $isPostLiked = $this->userRespository->getLikedPost($userId, $postId);
        if ($isPostLiked) {
            $user->removeGroup($post);
            $message = "You have unliked the post.";
        } else {
            $user->addPostLike($post);
            $message = "You have liked the post.";
        }
        
        $this->manager->persist($user);
        $this->manager->flush();

        return $message;
    }

    private function validate(Request $request)
    {
        //Validate
        $constraints = new Collection(array(
            'post_id' => new Assert\NotBlank(),
        ));

        $errors = $this->validator->validate(
            $request->request->all(),
            $constraints
        );

        return $errors;
    }
}
