<?php

namespace App\Request;

use Symfony\Component\HttpFoundation\Request;

class BaseRequest
{
    protected function transformJsonBody(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }
}
