<?php

namespace App\Request\User;

use App\Exception\CustomException;
use App\Exception\ValidationException;
use App\Repository\UserRepository;
use App\Request\BaseRequest;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\RequestStack;

class AuthRequest extends BaseRequest
{
    private $validator;
    private $request;
    private $userRepository;

    public function __construct(RequestStack $requestStack, ValidatorInterface $validator, UserRepository $userRepository)
    {
        $this->validator = $validator;
        $this->userRepository = $userRepository;
        $request = $requestStack->getCurrentRequest();
        $this->request = $this->transformJsonBody($request);
    }

    public function register()
    {
        //Validate
        $constraints = new Collection(array(
            'name' => new Assert\NotBlank(),
            'username' => new Assert\NotBlank(),
            'email' => array(
                new Assert\NotBlank(),
                new Assert\Email(),
            ),
            'password' => new Assert\NotBlank(),
        ));
        
        $errors = $this->validator->validate(
            $this->request->request->all(),
            $constraints
        );
        
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        $name = $this->request->get('name');
        $username = $this->request->get('username');
        $email = $this->request->get('email');
        $password = $this->request->get('password');

        $isUsernameFound = $this->userRepository->findOneBy(['username' => $username]);
        $isEmailFound = $this->userRepository->findOneBy(['email' => $email]);

        if ($isUsernameFound) {
            throw new CustomException("username is already exist!");
        }

        if ($isEmailFound) {
            throw new CustomException("email address is already exist!");
        }

        //Save
        $this->userRepository->saveUser($name, $username, $email, $password);
    }
}
