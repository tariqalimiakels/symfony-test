<?php

namespace App\Repository;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Post::class);
        $this->manager = $manager;
    }

    public function savePost($description, User $user)
    {
        $post = new Post();
        $post->setUser($user);
        $post->setDescription($description);
        $post->setStatus('active');

        $this->manager->persist($post);
        $this->manager->flush();
    }

    public function updatePost(Post $post): Post
    {
        $this->manager->persist($post);
        $this->manager->flush();

        return $post;
    }

    public function deletePost(Post $post)
    {
        $this->manager->remove($post);
        $this->manager->flush();
    }

    public function getWithSearchQueryBuilder()
    {
        $qb = $this->createQueryBuilder('c');
        $qb->orderBy('c.createdAt', 'DESC');

        return $qb;
    }

    
}
