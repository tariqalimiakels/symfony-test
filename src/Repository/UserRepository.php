<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    private $manager;
    private $encoder;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct($registry, User::class);
        $this->manager = $manager;
        $this->encoder = $encoder;
    }

    public function saveUser($name, $username, $email, $password)
    {
        $user = new User($username);
        $user->setName($name);
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $user->setStatus('active');

        $this->manager->persist($user);
        $this->manager->flush();
    }

    public function getLikedPost($userId, $postId)
    {
        $query = $this->createQueryBuilder('u')
            ->innerJoin('u.postLikes', 'l')
            ->andWhere('u.id = :user_id')
            ->andWhere('l.id = :post_id')
            ->setParameters(['user_id' => $userId, 'post_id' => $postId])
            ->getQuery()->getResult();

        return $query;
    }
}
