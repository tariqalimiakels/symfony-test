<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends AbstractController
{

    /**
     * @var integer HTTP status code - 200 (OK) by default
     */
    protected $statusCode = 200;

    /**
     * Gets the value of statusCode.
     *
     * @return integer
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Sets the value of statusCode.
     *
     * @param integer $statusCode the status code
     *
     * @return self
     */
    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Sets an error message and returns a JSON response
     *
     * @param string $errors
     * @param $headers
     * @return JsonResponse
     */
    public function respondWithErrors($errors, $headers = [])
    {
        $data = [
            'success' => false,
            'error' => [
                'message' => $errors,
                'code' => $this->getStatusCode(),
            ]
        ];

        return $this->renderResponse($data, $headers);
    }

    /**
     * Sets an error message and returns a JSON response
     *
     * @param mixed $data
     * @param $headers
     * @return JsonResponse
     */
    public function respondWithSuccess($data, $headers = [])
    {
        $data = [
            'success' => true,
            'data' => $data,
        ];

        return $this->renderResponse($data, $headers);
    }

    /**
     * Returns a 422 Unprocessable Entity
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondValidationError($message = 'Validation errors', $errors = [], $headers = [])
    {
        $data = [
            'success' => false,
            'error' => [
                'message' => $message,
                'code' => 422, // UNPROCESSABLE ENTITY
                'validated' => $errors,
            ]
        ];

        return $this->renderResponse($data, $headers);
    }

    /**
     * @param $jsonData
     * @param $status
     * @param array $headers
     * @return JsonResponse
     */
    private function renderResponse($jsonData, $headers = [])
    {
        return new JsonResponse($jsonData, $this->getStatusCode(), $headers);
    }

}