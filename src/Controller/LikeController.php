<?php
 
namespace App\Controller;

use App\Exception\CustomException;
use App\Exception\ValidationException;
use App\Request\Post\LikePostRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
  * Class LikeController
  * @package App\Controller
  * @Route("/api", name="like_api")
  */
 class LikeController extends ApiController
 {
     
     /**
      * @param LikePostRequest $likePostRequest
      * @return JsonResponse
      * @throws \Exception
      * @Route("/likes", name="likes_add_remove", methods={"POST"})
      */
     public function addRemoveLike(LikePostRequest $likePostRequest)
     {
         try {
             $userId = $this->getUser()->getId();
             $message = $likePostRequest->addOrRemoveLike($userId);
             return $this->respondWithSuccess($message);
         } catch (ValidationException $e) {
             return $this->respondValidationError("Validation failed!", $e->getMessages());
         } catch (CustomException $e) {
            return $this->respondWithErrors($e->getViolationMessage());
         } catch (\Exception $e) {
             return $this->respondWithErrors("Something went wrong!");
         }
     }
 }
