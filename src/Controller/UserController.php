<?php

namespace App\Controller;

use App\Exception\CustomException;
use App\Exception\ValidationException;
use App\Request\User\AuthRequest;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class UserController
 * @package App\Controller
 */
class UserController extends ApiController
{
    public function register(AuthRequest $authRequest): JsonResponse
    {
        try {
            $authRequest->register();
            return $this->respondWithSuccess('User successfully created.');
        } catch (CustomException $e) {
            return $this->respondWithErrors($e->getViolationMessage());
        } catch (ValidationException $e) {
            return $this->respondValidationError("Validation failed!", $e->getMessages());
        } catch (\Exception $e) {
            return $this->respondWithErrors("Something went wrong!");
        }
    }
}
