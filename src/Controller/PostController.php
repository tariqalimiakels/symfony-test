<?php
 
namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Exception\CustomException;
use App\Helpers\Constants;
use App\Repository\PostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Exception\ValidationException;
use App\Request\Post\DeletePostRequest;
use App\Request\Post\SavePostRequest;

/**
  * Class PostController
  * @package App\Controller
  * @Route("/api", name="post_api")
  */
 class PostController extends ApiController
 {
     private $postRepository;

     public function __construct(PostRepository $postRepository)
     {
         $this->postRepository = $postRepository;
     }

     /**
      * @param Request $request
      * @param PaginatorInterface $paginator
      * @return JsonResponse
      * @Route("/posts", name="posts", methods={"GET"})
      */
     public function getPosts(Request $request, PaginatorInterface $paginator)
     {
         $queryBuilder = $this->postRepository->getWithSearchQueryBuilder();
         $posts = $paginator->paginate(
             $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1),
             Constants::NUMBER_OF_ITEMS
         );

         $data = ['posts' => [], 'totalCount' => $posts->getTotalItemCount()];
         foreach ($posts as $post) {
             $data['posts'][] = $this->serializePost($post);
         }

         return $this->respondWithSuccess($data);
     }

     /**
      * @param SavePostRequest $savePostRequest
      * @return JsonResponse
      * @throws \Exception
      * @Route("/posts", name="posts_add", methods={"POST"})
      */
     public function addPost(SavePostRequest $savePostRequest)
     {
         try {
             $userId = $this->getUser()->getId();
             $savePostRequest->add($userId);

             return $this->respondWithSuccess('Post successfully created.');
         } catch (ValidationException $e) {
             return $this->respondValidationError("Validation failed!", $e->getMessages());
         } catch (\Exception $e) {
             return $this->respondWithErrors("Something went wrong!");
         }
     }

     /**
      * @param SavePostRequest $savePostRequest
      * @param $id
      * @return JsonResponse
      * @Route("/posts/{id}", name="posts_put", methods={"PUT"})
      */
     public function updatePost(SavePostRequest $savePostRequest, int $id)
     {
         try {
             $savePostRequest->update($id);
             return $this->respondWithSuccess('Post updated successfully.');
         } catch (ValidationException $e) {
             return $this->respondValidationError("Validation failed!", $e->getMessages());
         } catch (CustomException $e) {
            return $this->respondWithErrors($e->getViolationMessage());
         } catch (\Exception $e) {
             return $this->respondWithErrors("Something went wrong!");
         }
     }

     /**
      * @param DeletePostRequest $deletePostRequest
      * @param $id
      * @return JsonResponse
      * @Route("/posts/{id}", name="posts_delete", methods={"DELETE"})
      */
     public function deletePost(DeletePostRequest $deletePostRequest, $id)
     {
         try {
             $deletePostRequest->delete($id);
             return $this->respondWithSuccess('Post deleted successfully.');
         } catch (ValidationException $e) {
             return $this->respondValidationError("Validation failed!", $e->getMessages());
         } catch (CustomException $e) {
            return $this->respondWithErrors($e->getViolationMessage());
         } catch (\Exception $e) {
             dd($e);
             return $this->respondWithErrors("Something went wrong!");
         }
     }

     private function serializePost(Post $post)
     {
         $data = [
            'id' => $post->getId(),
            'description' => $post->getDescription(),
            'user' => [
                "id" => $post->getUser()->getId(),
                "name" => $post->getUser()->getName(),
            ],
            'comments' => [],
            'likes' => []
         ];

         foreach ($post->getComments() as $comment) {
             $data['comments'][] = $this->serializeComment($comment);
         }

         foreach ($post->getUserLikes() as $like) {
             $data['likes'][] = $this->serializeLikes($like);
         }

         return $data;
     }

     private function serializeComment(Comment $comment)
     {
         return [
            'comment' => $comment->getComment(),
            'user' => [
                "id" => $comment->getUser()->getId(),
                "name" => $comment->getUser()->getName(),
            ]
        ];
     }

     private function serializeLikes($like)
     {
         return [
            'user' => [
                'id' => $like->getId(),
                'name' => $like->getName()
            ]
        ];
     }
 }
