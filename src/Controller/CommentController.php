<?php
 
 namespace App\Controller;

 use App\Exception\CustomException;
 use App\Exception\ValidationException;
 use App\Request\Post\CommentPostRequest;
 use Symfony\Component\HttpFoundation\JsonResponse;
 use Symfony\Component\Routing\Annotation\Route;

 /**
  * Class CommentController
  * @package App\Controller
  * @Route("/api", name="comment_api")
  */
 class CommentController extends ApiController
 {
     
     /**
      * @param CommentPostRequest $commentPostRequest
      * @return JsonResponse
      * @throws \Exception
      * @Route("/comments", name="comment_add", methods={"POST"})
      */
     public function add(CommentPostRequest $commentPostRequest)
     {
         try {
             $userId = $this->getUser()->getId();
             $commentPostRequest->addComment($userId);
             return $this->respondWithSuccess('Comment successfully added.');
         } catch (ValidationException $e) {
             return $this->respondValidationError("Validation failed!", $e->getMessages());
         } catch (CustomException $e) {
             return $this->respondWithErrors($e->getViolationMessage());
         } catch (\Exception $e) {
             return $this->respondWithErrors("Something went wrong!");
         }
     }
 }
