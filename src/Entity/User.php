<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
    */
    private $username;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="post")
     */
    private $posts;

     /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="comment")
     */
    private $comments;

    /**
    * @ORM\ManyToMany(targetEntity="App\Entity\Post", inversedBy="userLikes")
    * @JoinTable(name="post_likes")
    */
    private $postLikes;

    /**
     * User constructor.
    * @param $username
    */
    public function __construct($username)
    {
        $this->username = $username;
        $this->posts = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->postLikes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username): void
    {
        $this->username = $username;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }


    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSalt()
    {
        return null;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @return Collection|Post[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addPostLike(Post $postLikes)
    {
        $postLikes->addUserLike($this);
        $this->postLikes->add($postLikes);

        return $this->postLikes;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPostLikes(): Collection
    {
        return $this->postLikes;
    }

    public function removeGroup(Post $postLikes)
    {
        $this->postLikes->removeElement($postLikes);
    }
}
