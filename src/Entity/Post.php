<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Post
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="user")
    */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="post", cascade={"remove"})
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="postLikes")
     */
    private $userLikes;

    /**
     * @ORM\Column(type="text")
     * @Groups({"tariffs"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $status;

    public function __construct()
    {
        $this->userLikes = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function addUserLike(User $userLikes)
    {
        $this->userLikes[] = $userLikes;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getUserLikes(): Collection
    {
        return $this->userLikes;
    }

    /**
     * @return Collection|Post[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }
}
