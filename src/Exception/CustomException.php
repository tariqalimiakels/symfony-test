<?php

namespace App\Exception;

class CustomException extends \Exception
{
    private $violation;

    /**
     * Constructor.
     *
     * @param array $violations Array with validation errors (violations).
     */
    public function __construct(String $violation)
    {
        $this->violation = $violation;
    }

    public function getViolationMessage(){
        return $this->violation;
    }

}
