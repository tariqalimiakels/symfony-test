<?php

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Our custom exception which mean that VO is invalid.
 *
 * The main purpose on this exception
 * is to store validation errors
 * and provide simple way to obtain this errors further.
 */
class ValidationException extends \Exception
{
    /**
     * This array contains validation errors (violations).
     *
     * @var array $violations Validation errors (violations). By default it's empty array.
     */
    private $violations;

    /**
     * Constructor.
     *
     * @param array $violations Array with validation errors (violations).
     */
    public function __construct($violations)
    {
        $this->violations = $violations;

        parent::__construct('validation failed.');
    }

    /**
     * Provides array with all validation errors (violations).
     *
     * IMPORTANT error message structure:
     * Each invalid parameter have array with error messages.
     *
     * @return array Array with errors messages.
     */
    public function getMessages()
    {
        $messages = [];

        /** @var ConstraintViolationList $violationList */
        foreach ($this->violations as $violation) {
            $field = preg_replace('/\[|\]/', "", $violation->getPropertyPath());
            $errorMessage = $violation->getMessage();
            $messages[$field][] = $errorMessage;
        }

        return $messages;
    }
}
