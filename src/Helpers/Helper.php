<?php

namespace App\Helpers;

use Carbon\Carbon;

class Helper
{
    /**
     * Get all validation errors
     *
     * @return array
     */
    public static function getValidationErrors($violationList)
    {
        $errors = array();
          foreach ($violationList as $violation){
            $field = preg_replace('/\[|\]/', "", $violation->getPropertyPath());
            $error = $violation->getMessage();
            $errors[$field] = $error;
          }

        return $errors;
    }

    /**
     * Get diff in minutes
     *
     * @return number
     */
    public static function getDiffInMinutes($oldDateTime){

      $date = Carbon::parse($oldDateTime);
      $now = Carbon::now();

      return $date->diffInMinutes($now);
    }
}
